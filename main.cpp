// Importing things
#include <fstream>
#include <iostream>

using namespace std;

// For clearing the input buffer
void NewLine() { cin.ignore(256, '\n'); }

int main() {
  // Variable declaration
  string name, fileName;
  int choice;

  // Welcome
  cout << "\n#####################################\n";
  cout << "## Welcome to cricket stats saver ##\n";
  cout << "#####################################\n\n";

  // Name
  cout << "Enter name: ";
  cin >> name;
  fileName = name.append(".txt");

  // Asking for choice
  cout << "\n1. Add a new score\n";
  cout << "2. Read score\n";
  cout << "3. Delete previous score\n";
  cout << "4. Exit\n";

  cout << "\nEnter choice: ";
  cin >> choice;

  // For adding a new score
  if (choice == 1) {
    // Variable declaration
    string date;
    int howManyMatch, howManyOvers;

    // File
    ofstream file;
    file.open(fileName, ios_base::app);

    // User input and adding content in file

    // Date
    cout << "\nEnter date when the match was played: ";
    NewLine();
    getline(cin, date);
    file << "Date: " << date;

    // How many match
    cout << "How many match did you played today (Enter a number): ";
    cin >> howManyMatch;

    // Match by Match asking info
    for (int i = 1; i <= howManyMatch; i++) {

      // Variable declaration
      char didBat, didBowl, didWon;

      file << "\n\nMatch " << i << endl;

      // How many over
      cout << "\nHow many overs match did you played in match " << i
           << " (Enter a number): ";
      cin >> howManyOvers;
      file << "Overs: " << howManyOvers << endl << endl;

      // Asking the user if he/she batted in the match
      cout << "\nDid you bat in this match? (y/n): ";
      cin >> didBat;

      // If batting was done in the match
      if (didBat == 'y' || didBat == 'Y') {

        // Variable declaration
        int howManyRuns, howManyBalls;
        double strikeRate;

        // How many runs made
        cout << "How many runs did you made? (Enter a number): ";
        cin >> howManyRuns;
        file << "Runs Made: " << howManyRuns << endl;

        // How many balls played
        cout << "How many balls did you played? (Enter a number): ";
        cin >> howManyBalls;
        file << "Balls Played: " << howManyBalls << endl;

        // Strike rate
        strikeRate = ((double)howManyRuns / howManyBalls) * 100.0;

        cout << "Strike Rate from which you played is " << strikeRate << endl;
        file << "Strike rate: " << strikeRate << endl;
      }

      // Asking the user if he/she bowled in the match
      cout << "\nDid you bowl in this match? (y/n): ";
      cin >> didBowl;

      // If bowling was done in the match
      if (didBowl == 'y' || didBowl == 'Y') {

        // Variable declaration
        char anyWicketsTaken, anyExtrasBowled;
        int howManyOvers, howManyWicket, howManyExtras, howManyRuns;
        double economy;

        // How many overs bowled
        cout
            << "How many over did you bowled in this match? (Enter a number): ";
        cin >> howManyOvers;
        file << "\nOvers bowled: " << howManyOvers << endl;

        // How many runs gone
        cout << "How many runs were gone? (Enter a number): ";
        cin >> howManyRuns;
        file << "Runs conceded: " << howManyRuns << endl;

        // Economy
        economy = (double)howManyRuns / howManyOvers;
        cout << "The economy that you bowled is " << economy << endl;
        file << "Economy: " << economy << endl;

        // How many wickets taken
        cout << "\nDid you take any wickets in this match (y/n): ";
        cin >> anyWicketsTaken;

        if (anyWicketsTaken == 'y' || anyWicketsTaken == 'Y') {
          cout << "How many wickets taken (Enter a number): ";
          cin >> howManyWicket;
          file << "Wickets: " << howManyWicket << endl;
        }

        // How many extras
        cout << "Did you bowled any extras (Wide/No ball) (y/n): ";
        cin >> anyExtrasBowled;

        if (anyExtrasBowled == 'y') {
          cout << "How many extras bowled: ";
          cin >> howManyExtras;
          file << "Extras: " << howManyExtras << endl;
        }
      }

      // Asking if they won the match or not
      cout << "Did you won the match (y/n): ";
      cin >> didWon;

      if (didWon == 'y' || didWon == 'Y') {
        file << "Win: Yes" << endl;
      }

      else {
        file << "Win: No" << endl;
      }
    }
    file << "________" << endl;

  }

  // For reading previous score
  else if (choice == 2) {
    cout << endl;
    string text;
    ifstream File(fileName);
    while (getline(File, text)) {
      cout << text << endl;
    }
    File.close();
    cout << endl;
  }

  // For deleting previous score
  else if (choice == 3) {
    char choice;

    cout << "Are you sure that you want to delete the file (y/n): ";
    cin >> choice;

    if (choice == 'y' || choice == 'Y') {
      int isDeleted = remove(fileName.c_str());
    }

  }
  // Quitting from the program
  else if (choice == 4) {
    exit(0);
  }

  // Do if wrong choice is added
  else {
    cout << "Wrong choice";
  }

  return 0;
}